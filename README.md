# Moodle Field Widget

Provides a Moodle Field Widget for string field types which allows selection of
a Moodle LMS Course.

The field widget uses a Service which provides some basic integration with the
[Moodle Web service API](https://docs.moodle.org/dev/Web_service_API_functions).

## Configuration

A configuration form `/admin/config/services/moodle` allows configuration of
the API URL and Token.

Instances of the Field Widget allow configuration of a `category` which will be
used to limit courses returned from the API.

## Webservice API support

Currently, the following functions are supported with bespoke methods:

- `core_course_get_courses_by_field`
- `core_user_get_users_by_field`
- `core_user_create_users`
- `enrol_manual_enrol_users`
- `core_group_add_group_members`
- `core_group_get_course_groups`
- `core_group_get_group_members`

You may use the supplied `makeApiRequest` method to instantiate further
functions.
