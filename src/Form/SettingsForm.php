<?php

namespace Drupal\moodle_field_widget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Moodle Field Widget settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'moodle_field_widget_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['moodle_field_widget.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['api_url'] = [
      '#type' => 'url',
      '#title' => $this->t('API URL'),
      '#default_value' => $this->config('moodle_field_widget.settings')->get('api_url'),
      '#description' => $this->t('An absolute URL for the Moodle API endpoint.'),
    ];
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
      '#default_value' => $this->config('moodle_field_widget.settings')->get('api_token'),
      '#description' => $this->t('Token used to authenticate requests to the API.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('moodle_field_widget.settings')
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('api_token', $form_state->getValue('api_token'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
