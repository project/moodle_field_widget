<?php

namespace Drupal\moodle_field_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\moodle_field_widget\MoodleService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'moodle_field_widget_course' field widget.
 *
 * @FieldWidget(
 *   id = "moodle_field_widget_course",
 *   label = @Translation("Moodle Course"),
 *   field_types = {"string"},
 * )
 */
class MoodleFieldWidgetCourse extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * Service for Moodle.
   *
   * @var \Drupal\moodle_field_widget\MoodleService
   */
  private MoodleService $moodleService;

  /**
   * Module Handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    MoodleService $moodle_service,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings
    );

    $this->moodleService = $moodle_service;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
                       $plugin_id,
                       $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('moodle_field_widget.service'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'category' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'category' => [
        '#type' => 'textfield',
        '#title' => $this->t('Filter courses by category'),
        '#default_value' => $this->getSetting('category'),
        '#description' => $this->t('Enter a value to be passed to the Moodle API as a category filter.'),
      ],
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @todo Add logic to get courses from the Moodle Service and put them
   *    into options.
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $category = $this->getSetting('category');

    $element['value'] = $element + [
      // Use select2 if it's available.
      '#type' => ($this->moduleHandler->moduleExists('select2') ? 'select2' : 'select'),
      '#options' => [],
      '#default_value' => $items[$delta]->value ?? NULL,
    ];

    // Get the courses from the Moodle API.
    $courses = $this->moodleService->getCoursesByField('category', $category);

    // Add them to the select options.
    foreach ($courses as $course) {
      $option_label = $this->t('@title (@id)', [
        '@title' => $course['displayname'],
        '@id' => $course['id'],
      ]);
      $element['value']['#options'][$course['id']] = $option_label;
    }

    if (empty($element['value']['#options'])) {
      // Let the user know that there actually aren't any courses to display -
      // this is not an error.
      $this->messenger()->addWarning($this->t("Moodle LMS did not return any courses."));
    }

    return $element;
  }

}
