<?php

namespace Drupal\moodle_field_widget;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Provides methods for interacting with the Moodle API.
 */
class MoodleService implements LoggerAwareInterface {

  use StringTranslationTrait;
  use LoggerAwareTrait;

  /**
   * The moodle configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a MoodleService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('moodle_field_widget.settings');
    $this->httpClient = $http_client;
  }

  /**
   * Make a Moodle API request.
   *
   * Sample usage:
   * ```php
   *   $this->makeApiRequest('core_course_get_courses_by_field', [
   *     'field' => 'category',
   *     'value' => 123,
   *   ]);
   * ```
   *
   * @param string $function
   *   The webservice function. E.g. 'core_course_get_courses_by_field'.
   * @param array $params
   *   Additional parameters for the request. 'moodlewsrestformat', 'wstoken'
   *   and 'wsfunction' parameters are automatically populated.
   *
   * @return mixed
   *   Data returned from the request.
   */
  public function makeApiRequest(string $function, array $params = []): mixed {
    // Get the API url.
    if (!$this->config->get('api_url')) {
      // No API url, log an error.
      $this->logger->error($this->t('Unable to make a Moodle API request because no Moodle Webservice API URL has been configured.'));
      return FALSE;
    }

    $api_url = $this->config->get('api_url');

    // Gather all the parameters that need to be added to our query.
    // Note that the 'moodlewsrestformat', 'wstoken' and 'wsfunction'
    // parameters must be the first, second and third parameters respectively.
    $params = array_merge(
      [
        'moodlewsrestformat' => 'json',
        'wstoken' => $this->config->get('api_token'),
        'wsfunction' => $function,
      ],
      $params
    );

    // Concatenate our query parameters onto the URL.
    if ($params) {
      $query_parts = [];

      foreach ($params as $param_key => $param_value) {
        $query_parts[] = urlencode($param_key) . '=' . urlencode($param_value);
      }

      $api_url .= '?' . implode('&', $query_parts);
    }

    // Send the request.
    try {
      // Make an API request using the HTTP client.
      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->request('POST', $api_url);

      // Get the result.
      $result = Json::decode(
        $response->getBody()->getContents()
      );

      // Log an error if the response status code is not 200 (OK).
      if ($response->getStatusCode() != 200) {
        $this->logger->error('Request to the Moodle Webservice @endpoint endpoint received a not-OK response code: @code', [
          '@context' => $response->getStatusCode(),
          '@endpoint' => $api_url,
        ]);
      }

      // If the call returns an error message, log that.
      if ($result && isset($result['exception'])) {
        $this->logger->error('Request to the Moodle Webservice @endpoint endpoint returned an exception: @exception; message: @message', [
          '@endpoint' => $api_url,
          '@exception' => $result['exception'],
          '@message' => $result['message'] ?? '',
        ]);

        return FALSE;
      }
    }
    catch (GuzzleException $e) {
      $this->logger->error('Error performing HTTP request to Moodle Webservice @endpoint endpoint: @context', [
        '@context' => $e->getMessage(),
        '@endpoint' => $api_url,
      ]);
    }
    catch (InvalidDataTypeException $e) {
      $this->logger->error('Error decoding JSON returned by Moodle Webservice @endpoint endpoint: @context', [
        '@context' => $e->getMessage(),
        '@endpoint' => $api_url,
      ]);
    }
    catch (\Exception $e) {
      $this->logger->error('Unexpected exception performing request to Moodle Webservice @endpoint endpoint: @context', [
        '@context' => $e->getMessage(),
        '@endpoint' => $api_url,
      ]);
    }

    return $result;
  }

  /**
   * Gets information about a Moodle course.
   *
   * @param string $field
   *   The Moodle field to search on. E.g. 'category'.
   * @param string $value
   *   The value to search for.
   *
   * @return array
   *   An array of courses.
   */
  public function getCoursesByField(string $field, string $value = ''): array {
    $params = [];

    if ($field) {
      $params = [
        'field' => $field,
        'value' => $value ?? '',
      ];
    }

    $data = $this->makeApiRequest('core_course_get_courses_by_field', $params);

    // Return the courses.
    if (isset($data['courses'])) {
      return $data['courses'];
    }

    return [];
  }

  /**
   * Gets information about a Moodle groups by course.
   *
   * @param string $courseid
   *   The courseid of the Moodle course.
   *
   * @return array
   *   An array of groups.
   */
  public function getCourseGroupsByCourseId(string $courseid): array {
    $params = [
      'courseid' => $courseid,
    ];

    return $this->makeApiRequest('core_group_get_course_groups', $params);
  }

  /**
   * Gets information about a Moodle user.
   *
   * @param string $field
   *   The Moodle field to search on. E.g. 'id', 'username' or 'email'.
   * @param array $values
   *   An array of values to search for. E.g. 'john.doe@gmail.com'. Note that
   *   not all fields accept more than one field - in that case only the last
   *   item will be matched.
   *
   * @return array|false
   *   An array of users, or FALSE.
   */
  public function getUsersByField(string $field, array $values) {
    $params = [];

    if ($field) {
      $params = [
        'field' => $field,
      ];

      // The values key can be an array.
      foreach ($values as $key => $value) {
        $params["values[$key]"] = $value;
      }
    }

    $data = $this->makeApiRequest('core_user_get_users_by_field', $params);

    // Return the users.
    if (!$data) {
      return FALSE;
    }

    return $data;
  }

  /**
   * Create one or more Moodle users.
   *
   * @param array $users
   *   An array of users to create on Moodle. Each user is an array with keys
   *   for 'username', 'auth', 'email', 'firstname' and 'lastname'. Moodle
   *   username policy only allows alphanumeric lowercase characters
   *   (letters/numbers) and underscore ( _ ) and hyphen( - ) and period( . )
   *   and at-symbol( @ )
   *
   * @return array|false
   *   An array with keys for 'id' and 'username' if successful, and false
   *   otherwise.
   */
  public function createUsers(array $users) {
    $params = [];

    foreach ($users as $key => $user) {
      foreach ($user as $property => $value) {
        $params["users[$key][$property]"] = $value;
      }
    }

    $data = $this->makeApiRequest('core_user_create_users', $params);

    if (!$data) {
      return FALSE;
    }

    return $data;
  }

  /**
   * Enrols one or more Moodle users to a Moodle course.
   *
   * @param array $enrolments
   *   An array of enrolments. Each enrollment is an array with keys
   *   for:
   *   - courseid: The Moodle ID of the course to enrol upon.
   *   - userid: The Moodle ID of the user who is enrolling.
   *   - roleid: The role to assign to the enrolling user .
   *
   * @returns bool
   *    Whether the operation succeeded.
   */
  public function enrolUsers(array $enrolments): bool {
    $params = [];

    foreach ($enrolments as $key => $enrolment) {
      foreach ($enrolment as $property => $value) {
        $params["enrolments[$key][$property]"] = $value;
      }
    }

    $response = $this->makeApiRequest('enrol_manual_enrol_users', $params);

    if ($response !== NULL) {
      // enrol_manual_enrol_users returns NULL in the event of success.
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Adds one or more Moodle users to a Moodle course group.
   *
   * @param array $members
   *   An array of group members. Each member is an array with keys
   *   for:
   *   - groupid: The Moodle ID of the group to add to.
   *   - userid: The Moodle ID of the user who is enrolling.
   *
   * @returns bool
   *    Whether the operation succeeded.
   */
  public function addMemberToGroup(array $members): bool {
    $params = [];

    foreach ($members as $key => $member) {
      foreach ($member as $property => $value) {
        $params["members[$key][$property]"] = $value;
      }
    }

    $response = $this->makeApiRequest('core_group_add_group_members', $params);

    if ($response !== NULL) {
      // core_group_add_group_members returns NULL in the event of success.
      return FALSE;
    }

    return TRUE;
  }

}
